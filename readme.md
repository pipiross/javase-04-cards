# Tasks
* (_elementary_) Provide an implementation that passes all tests.
* (_intermediate_) Provide an implementation using the Streams API (control statements are not allowed).
* (_intermediate_) Provide an implementation based on external library.
* (_intermediate_) Add parameterized tests.
