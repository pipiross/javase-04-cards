package hrytsenko.example;

import org.junit.Assert;
import org.junit.Test;

public class CardsTest {

    @Test
    public void validateNumber_visa_valid() {
        Assert.assertTrue(Cards.validateNumber("4485312678642542"));
    }

    @Test
    public void validateNumber_visaElectron() {
        Assert.assertTrue(Cards.validateNumber("4913718053571357"));
    }

    @Test
    public void validateNumber_masterCard() {
        Assert.assertTrue(Cards.validateNumber("5405827821668072"));
    }

    @Test
    public void validateNumber_americanExpress() {
        Assert.assertTrue(Cards.validateNumber("371185987578861"));
    }

    @Test
    public void validateNumber_invalidNumber() {
        Assert.assertFalse(Cards.validateNumber("1111111111111111"));
    }

}
